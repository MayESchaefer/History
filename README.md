# History
A really simple generic implementation of a memento history class in C#. The methods Do, Undo, and Redo store and retrieve the states of the history, and the Clear method (and its overloads) to manage the stack itself.
