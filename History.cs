﻿using System;
using System.Collections.Generic;

namespace Hate9
{
    /// <summary>A really simple generic implementation of a memento history class. The methods Do, Undo, and Redo store and retrieve the states of the history.</summary>
    public static class History
    {
        //name, index, type, values
        private static readonly List<(string, int, Type, List<object>)> histories = new List<(string, int, Type, List<object>)>();

        private static void UpdateHistory(this int index, int historyIndex, List<object> historyValues = null)
        { 
            histories[index] = (histories[index].Item1, historyIndex, histories[index].Item3, historyValues ?? new List<object>());
        }

        /// <summary>Adds the specified value to the history stack with the name of the referenced variable.</summary>
        /// <param name="value">The value and name to be added to the history stacks.</param>
        public static void Do<T>(ref T value)
        {
            Do(value, nameof(value));
        }

        /// <summary>Adds the specified value to the history stack with the specified name.</summary>
        /// <param name="value">The value to be added to the stack.</param>
        /// <param name="name">The name of the history stack to be added to.</param>
        public static void Do<T>(T value, string name)
        {
            while (!histories.Exists(x => x.Item1 == name && x.Item3 == value.GetType()))
            {
                histories.Add((name, 0, value.GetType(), new List<object>()));
            }
            int index = histories.FindIndex(x => x.Item1 == name);
            int historyIndex = histories[index].Item2;
            List<object> historyValues = histories[index].Item4;

            if (historyIndex > 0)
            {
                historyValues.RemoveRange(0, historyIndex);
            }
            historyIndex = 0;
            historyValues.Insert(0, value);

            index.UpdateHistory(historyIndex, historyValues);
        }

        /// <summary>Sets the specified variable to the previous entry in the history stack with the name of the referenced variable.</summary>
        /// <param name="value">The variable to be set.</param>
        public static void Undo<T>(ref T value)
        {
            Undo(ref value, nameof(value));
        }

        /// <summary>Sets the specified variable to the previous entry in the history stack with the specified name.</summary>
        /// <param name="value">The variable to be set.</param>
        /// <param name="name">The name of the history stack to be accessed.</param>
        public static void Undo<T>(ref T value, string name)
        {
            T val = value;
            if (histories.Exists(x => x.Item1 == name && x.Item3 == val.GetType()))
            {
                int index = histories.FindIndex(x => x.Item1 == name);
                int historyIndex = histories[index].Item2;
                if (historyIndex < histories[index].Item4.Count - 1)
                {
                    historyIndex++;
                    value = (T)histories[index].Item4[historyIndex];
                }

                index.UpdateHistory(historyIndex);
            }
        }


        /// <summary>Sets the specified variable to the next entry in the history stack with the name of the referenced variable.</summary>
        /// <param name="value">The variable to be set.</param>
        public static void Redo<T>(ref T value)
        {
            Redo(ref value, nameof(value));
        }

        /// <summary>Sets the specified variable to the previous entry in the history stack with the specified name.</summary>
        /// <param name="value">The variable to be set.</param>
        /// <param name="name">The name of the history stack to be accessed.</param>
        public static void Redo<T>(ref T value, string name)
        {
            T val = value;
            if (histories.Exists(x => x.Item1 == name && x.Item3 == val.GetType()))
            {
                int index = histories.FindIndex(x => x.Item1 == name);
                int historyIndex = histories[index].Item2;
                if (historyIndex > 0)
                {
                    historyIndex--;
                    value = (T)histories[index].Item4[historyIndex];
                }

                index.UpdateHistory(historyIndex);
            }
        }

        /// <summary>Clears all of the history stacks.</summary>
        public static void Clear()
        {
            histories.Clear();
        }

        /// <summary>Removes the history stack with the name of the specified variable.</summary>
        /// <param name="value">The variable to be removed from the history stacks.</param>
        /// <returns>Whether or not the stack was successfully removed.</returns>
        public static bool Clear<T>(ref T value)
        {
            return Clear(ref value, nameof(value));
        }

        /// <summary>Removes the history stack with the specified name and the type of the specified variable.</summary>
        /// <param name="value">The variable with the type to be removed from the history stacks.</param>
        /// <param name="name">The name of the history stack to be removed.</param>
        /// <returns>Whether or not the stack was successfully removed.</returns>
        public static bool Clear<T>(ref T value, string name)
        {
            try
            {
                T val = value;
                if (histories.Exists(x => x.Item1 == name && x.Item3 == val.GetType()))
                {
                    histories.RemoveAt(histories.FindIndex(x => x.Item1 == name));
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
